/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AppRegistry} from 'react-native';
import DeviceInfo from 'react-native-device-info';
/* create mqtt client */
const uniqueId = DeviceInfo.getUniqueID();

/*mqtt.createClient({
    uri: 'dev-api.postown.net:8883',
    user: 'granada:granadaMobileMQTTUser',
    pass: 'FLdS9@c2R50PJwV8K0fi1',
    clientId: 'mobile-' + uniqueId,*/


import {Client, Message} from 'react-native-paho-mqtt';

//Set up an in-memory alternative to global localStorage
const myStorage = {
    setItem: (key, item) => {
        myStorage[key] = item;
    },
    getItem: (key) => myStorage[key],
    removeItem: (key) => {
        delete myStorage[key];
    },
};


// Create a client instance

//const client = new Client({ uri: 'ws://iot.eclipse.org:80/ws', clientId: 'clientId', storage: myStorage });


const client = new Client({
    //uri: 'ssl://dev-api.postown.net:8883/ws',
    uri: 'ws://192.168.1.165:8883/ws',
    clientId: 'mobile-99c3c0fc-a9d4-3faa-94dc-37ed37267e66',
    storage: myStorage
});

/*const client = new Client({
    uri: 'ssl://dev-api.postown.net:8883',
    clientId: 'mobile-99c3c0fc-a9d4-3faa-94dc-37ed37267e77',
    storage: myStorage,

})*/
//
/*const client = new Client({ uri: '192.168.1.165:8883', clientId: 'mobile-99c3c0fc-a9d4-3faa-94dc-37ed37267e66', storage: myStorage,


});*/

// set event handlers
client.on('connectionLost', (responseObject) => {
    if (responseObject.errorCode !== 0) {
        console.log(responseObject.errorMessage);
    }
});
client.on('messageReceived', (message) => {
    console.log(message.payloadString);
});

// connect the client
client.connect({

    /*
       userName,
       password,
       willMessage: willMessage || null,
       timeout,
       keepAliveInterval,
       cleanSession,
       mqttVersion: mqttVersion === 4 ? 4 : 3,
       onSuccess: resolve,
       onFailure: reject

           options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setKeepAliveInterval(60 * 2);   // Default 60 seconds
            options.setUserName("granada:granadaMobileMQTTUser");
            options.setPassword("FLdS9@c2R50PJwV8K0fi1".toCharArray());


   */
    userName: 'granada:granadaMobileMQTTUser',
    password: 'FLdS9@c2R50PJwV8K0fi1'.split('').join(','),
    mqttVersion: 3,
    keepAliveInterval: 60 * 2,
    cleanSession: true,

})
    .then(() => {
        // Once a connection has been made, make a subscription and send a message.
        console.log('onConnect');
        return client.subscribe('World');
    })
    .then(() => {
        const message = new Message('Hello');
        message.destinationName = 'World';
        client.send(message);
    })
    .catch((responseObject) => {
        if (responseObject.errorCode !== 0) {
            console.log('onConnectionLost:' + responseObject.errorMessage);
        }
    })
;

type Props = {};
export default class App_backupuupupup extends Component<Props> {


    componentDidMount() {
    }


    render() {
        return (
            <View style={styles.container}>
                <Text>234234234234234</Text>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


AppRegistry.registerComponent('App', () => App);