const extraNodeModules = require('node-libs-browser')

// add any customisations
extraNodeModules.net = require.resolve('react-native-tcp')

module.exports = {
    extraNodeModules
}