/**
 * @format
 */
import './globals'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// import this at the top

// import your app code after globals.js


AppRegistry.registerComponent(appName, () => App);
